﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.Services;
using System.Web;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.IO;

namespace Activity
{
    public partial class Form1 : Form
    {
        string catalog = null;
        int coutFigures = 0;
        Diagram diagram = new Diagram();
        string filename = null;
        int index = 0;
        bool isUndo = false;
        private FormSettingStore formSettings;
        public Form1()
        {
           
            InitializeComponent();
            formSettings = new FormSettingStore(@"Software\MyApp\", this.Name);
            formSettings.ApplySettings(this, catalog);
            miNewDiagram_Click(null, null);
            
            catalog = null;

            ScrollProperties vScroll = controlDiagrama1.VerticalScroll;
            vScroll.SmallChange = 1;
            vScroll.LargeChange = 1;
            ScrollProperties hScroll = controlDiagrama1.HorizontalScroll;
         
           
        }
        private void miNewDiagram_Click(object sender, EventArgs e)
        {
            diagram = new Diagram();
            controlDiagrama1.Diagram = diagram;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //formSettings.SaveSettings(this);
            this.Close();
            this.Dispose();
        }
        
        private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            if (e.Button == tblSave)
                CommandSave();
            else if (e.Button == tblUndo)
                CommandUndo();
            else if (e.Button == tblNotUndo)
                CommandRedo();
            else if (e.Button == tblDelete)
                CommandDelete();
            else if (e.Button == tblHelp)
                CommandHelp();
            else if (e.Button == tblNext)
                CommandNext();
            else if (e.Button == tblRetangle)
                CommandRetangle();
            else if (e.Button == tblRetangle1)
                CommandRetangle1();
            else if (e.Button == tblArrow)
                CommandArrow();
            else if (e.Button == tblDiamond)
                CommandDiamond();
            else if (e.Button == tblleroms)
                Commandleroms();
        }
        private void Commandleroms()
        {
            if (controlDiagrama1.SelectedFigure != null && controlDiagrama1.SelectedFigure is SolidFigure)
            {
                LinePerelom lagLine = new LinePerelom();
                lagLine.From = controlDiagrama1.SelectedFigure as SolidFigure;

                EndLineMarker market = new EndLineMarker(diagram, 10);
                market.location = lagLine.From.location;
                market.location.Offset((int)lagLine.From.Size.Width + 20, (int)lagLine.From.Size.Height / 2 - 10);
                lagLine.To = market;

                diagram.figures.Add(lagLine);
                controlDiagrama1.SelectedFigure = lagLine;
                controlDiagrama1.AddShagUndo();
                controlDiagrama1.CreateMarker();
                isUndo = false;
                Invalidate();
            }
        }
        private void CommandSave()
        {
            if (filename != null)
                diagram.Save(filename);
            else
            {
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    diagram.Save(saveFileDialog1.FileName);
                filename = saveFileDialog1.FileName;
            }
        }
        private void CommandUndo()
        {
            if (!isUndo)
            {
                controlDiagrama1.UndoClick(null, null);
                isUndo = true; 
            }
        }
        private void CommandRedo()
        {
            controlDiagrama1.RedoClick(null, null);
            isUndo = false;
        }
        private void CommandDelete()
        {
            controlDiagrama1.удалитьОбъектToolStripMenuItem_Click(null, null);
            coutFigures--;
            isUndo = false;
        }
        private void CommandHelp()
        {

        }
        public void getValue(string str)
        {

        }

        private void CommandRetangle()
        {
            if (coutFigures < 50)
            {
                isUndo = false;
                Point locations = new Point(40, 40);
                controlDiagrama1.AddFigure<RectangleFigure>(locations);
                coutFigures++;
            }
        }
        private void CommandRetangle1()
        {
            if (coutFigures < 50)
            {
                isUndo = false;
                Point locations = new Point(40, 40);
                controlDiagrama1.AddFigure<RoundRectFigure>(locations);
                coutFigures++;
            }
        }
        private void CommandArrow()
        {

            AssocFigure line = new AssocFigure();
            if (controlDiagrama1.SelectedFigure != null && controlDiagrama1.SelectedFigure is SolidFigure)
            {
                isUndo = false;
                line.From = controlDiagrama1.SelectedFigure as SolidFigure;
                EndLineMarker market = new EndLineMarker(diagram, 1);
                market.location = line.From.location;
                market.location.Offset(0, (int)line.From.Size.Height / 2 + 40);
                line.To = market;

                diagram.figures.Add(line);
                controlDiagrama1.SelectedFigure = line;
                controlDiagrama1.AddShagUndo();
                controlDiagrama1.CreateMarker();
                Invalidate();

            }
        }
        private void CommandNext()
        {

            if (controlDiagrama1.SelectedFigure != null && controlDiagrama1.SelectedFigure is SolidFigure)
            {
                isUndo = false;
                LedgeLineFigure lagLine = new LedgeLineFigure();
                lagLine.From = controlDiagrama1.SelectedFigure as SolidFigure;

                EndLineMarker market = new EndLineMarker(diagram, 1);
                market.location = lagLine.From.location;
                market.location.Offset(0, (int)lagLine.From.Size.Height / 2 + 40);
                lagLine.To = market;

                diagram.figures.Add(lagLine);
                controlDiagrama1.SelectedFigure = lagLine;
                controlDiagrama1.AddShagUndo();
                controlDiagrama1.CreateMarker();

                Invalidate();
            }
        }
        private void CommandDiamond()
        {
            if (coutFigures < 50)
            {
                isUndo = false;
                Point locations = new Point(40, 40);
                controlDiagrama1.AddFigure<RhombFigure>(locations);
                coutFigures++;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                diagram = Diagram.Load(openFileDialog1.FileName);
                controlDiagrama1.Diagram = diagram;
            }
        }



        private void openToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                diagram = Diagram.Load(openFileDialog1.FileName);
                catalog = openFileDialog1.FileName; // luu lai catalog of the file
                controlDiagrama1.Diagram = diagram;
                controlDiagrama1.market.Clear();
                controlDiagrama1.ShagRedo.Clear();
                controlDiagrama1.ShagUndo.Clear();
                coutFigures = controlDiagrama1.CountFiguresNows();
            }
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            formSettings.SaveSettings(this, catalog);
            this.Close();
            this.Dispose();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CommandSave();

        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            diagram = new Diagram();
            controlDiagrama1.Diagram = diagram;
        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            if (saveFileDialog2.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog2.FilterIndex == 1)
                {
                    string paths = saveFileDialog2.FileName;
                    controlDiagrama1.GetImage().Save(saveFileDialog2.FileName);           
                }
                if (saveFileDialog2.FilterIndex == 2)
                    controlDiagrama1.SaveAsMetafile(saveFileDialog2.FileName);
            }
        }

        private void controlDiagrama1_Load(object sender, EventArgs e)
        {

        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controlDiagrama1.удалитьОбъектToolStripMenuItem_Click(sender, e);
            coutFigures--;
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controlDiagrama1.RedoClick(null, null);
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controlDiagrama1.UndoClick(null, null);
        }

        private void controlDiagrama1_Load_1(object sender, EventArgs e)
        {

        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                diagram.Save(saveFileDialog1.FileName);
                filename = saveFileDialog1.FileName;
            }
        }

        private void экспортВJMGСВодянымЗнакомToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string st = null;
            string image = "imgage.png" + index.ToString() ;
            index++;
            if (saveFileDialog2.ShowDialog() == DialogResult.OK)
            {
                if (saveFileDialog2.FilterIndex == 1)
                {
                    string paths = saveFileDialog2.FileName;
                    controlDiagrama1.GetImage().Save(image);
                    st = Application.StartupPath;
                    System.Drawing.Image backImg = System.Drawing.Image.FromFile(image);
                    System.Drawing.Image ghostImg = System.Drawing.Image.FromFile(st + "/Untitled.png"); // водяной знак
                    System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(backImg);
                    Bitmap transparentGhost = new Bitmap(ghostImg.Width, ghostImg.Height);
                    Graphics transGraphics = Graphics.FromImage(transparentGhost);
                    ColorMatrix tranMatrix = new ColorMatrix();
                    tranMatrix.Matrix33 = 0.35F;  // устанавливаем прозрачность

                    ImageAttributes transparentAtt = new ImageAttributes();
                    transparentAtt.SetColorMatrix(tranMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                    transGraphics.DrawImage(ghostImg, new Rectangle(0, 0, transparentGhost.Width, transparentGhost.Height), 0, 0, transparentGhost.Width, transparentGhost.Height, GraphicsUnit.Pixel, transparentAtt);
                    transGraphics.Dispose();

                    g.DrawImage(transparentGhost, 0, 0); // координаты размищения водяного знака
                    //  File.Delete(paths);

                    backImg.Save(paths);
                    File.Delete(st + image);
                }
            }
           

        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controlDiagrama1.CopyToClipBoard();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isUndo = false;
            controlDiagrama1.Paste();
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            controlDiagrama1.selectAllFigures();
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isUndo = false;
            controlDiagrama1.CutFigure(null, null);
        }

        private void выкладываниеФайлНаСерверToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //string st = Path.GetFullPath("");
          //  MessageBox.Show(st);
           // string st = Application.StartupPath;
           // MessageBox.Show(st);
            UploadForm formUp = new UploadForm();
            formUp.Show();
            //formUp.Dispose();
        }

        private void Form1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void шаблон2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string st = Application.StartupPath;
            diagram = Diagram.Load(st + "/шаблон2.diagram");
            controlDiagrama1.Diagram = diagram;
            controlDiagrama1.market.Clear();
            controlDiagrama1.ShagRedo.Clear();
            controlDiagrama1.ShagUndo.Clear();
        }

        private void шаблон1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string st = Application.StartupPath;
            diagram = Diagram.Load(st + "/шаблон1.diagram");
            controlDiagrama1.Diagram = diagram;
            controlDiagrama1.market.Clear();
            controlDiagrama1.ShagRedo.Clear();
            controlDiagrama1.ShagUndo.Clear();
        }

        private void шаблон3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string st = Application.StartupPath;
            diagram = Diagram.Load(st + "/шаблон3.diagram");
            controlDiagrama1.Diagram = diagram;
            controlDiagrama1.market.Clear();
            controlDiagrama1.ShagRedo.Clear();
            controlDiagrama1.ShagUndo.Clear();
        }

        private void controlDiagrama1_Load_2(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            formSettings.SaveSettings(this, catalog);
        }

        private void controlDiagrama1_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.ResetTransform();
        }
       
      
    }
}