﻿namespace Activity
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выходToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.экспортВJMGСВодянымЗнакомToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выкладываниеФайлНаСерверToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьПоШалбону1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.шаблон1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.шаблон2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.шаблон3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBar1 = new System.Windows.Forms.ToolBar();
            this.tblSave = new System.Windows.Forms.ToolBarButton();
            this.tblUndo = new System.Windows.Forms.ToolBarButton();
            this.tblNotUndo = new System.Windows.Forms.ToolBarButton();
            this.tblDelete = new System.Windows.Forms.ToolBarButton();
            this.tblHelp = new System.Windows.Forms.ToolBarButton();
            this.tblNext = new System.Windows.Forms.ToolBarButton();
            this.tblRetangle = new System.Windows.Forms.ToolBarButton();
            this.tblRetangle1 = new System.Windows.Forms.ToolBarButton();
            this.tblArrow = new System.Windows.Forms.ToolBarButton();
            this.tblDiamond = new System.Windows.Forms.ToolBarButton();
            this.tblleroms = new System.Windows.Forms.ToolBarButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            this.controlDiagrama1 = new Activity.ControlDiagrama();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(696, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.toolStripSeparator,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator1,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem,
            this.выходToolStripMenuItem,
            this.экспортВJMGСВодянымЗнакомToolStripMenuItem,
            this.выкладываниеФайлНаСерверToolStripMenuItem,
            this.создатьПоШалбону1ToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.fileToolStripMenuItem.Text = "Файл";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("newToolStripMenuItem.Image")));
            this.newToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.newToolStripMenuItem.Text = "Создать";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.openToolStripMenuItem.Text = "Открыть";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click_1);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(297, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.saveToolStripMenuItem.Text = "Сохранить";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.S)));
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.saveAsToolStripMenuItem.Text = "Сохранить как ...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(297, 6);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(297, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.exitToolStripMenuItem.Text = "Экспорт в .png";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click_1);
            // 
            // выходToolStripMenuItem
            // 
            this.выходToolStripMenuItem.Name = "выходToolStripMenuItem";
            this.выходToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.выходToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.выходToolStripMenuItem.Text = "Выход";
            this.выходToolStripMenuItem.Click += new System.EventHandler(this.выходToolStripMenuItem_Click);
            // 
            // экспортВJMGСВодянымЗнакомToolStripMenuItem
            // 
            this.экспортВJMGСВодянымЗнакомToolStripMenuItem.Name = "экспортВJMGСВодянымЗнакомToolStripMenuItem";
            this.экспортВJMGСВодянымЗнакомToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.экспортВJMGСВодянымЗнакомToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.экспортВJMGСВодянымЗнакомToolStripMenuItem.Text = "Экспорт в .png с водяным знаком";
            this.экспортВJMGСВодянымЗнакомToolStripMenuItem.Click += new System.EventHandler(this.экспортВJMGСВодянымЗнакомToolStripMenuItem_Click);
            // 
            // выкладываниеФайлНаСерверToolStripMenuItem
            // 
            this.выкладываниеФайлНаСерверToolStripMenuItem.Name = "выкладываниеФайлНаСерверToolStripMenuItem";
            this.выкладываниеФайлНаСерверToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.выкладываниеФайлНаСерверToolStripMenuItem.Text = "Выкладывание файла на сервер";
            this.выкладываниеФайлНаСерверToolStripMenuItem.Click += new System.EventHandler(this.выкладываниеФайлНаСерверToolStripMenuItem_Click);
            // 
            // создатьПоШалбону1ToolStripMenuItem
            // 
            this.создатьПоШалбону1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.шаблон1ToolStripMenuItem,
            this.шаблон2ToolStripMenuItem,
            this.шаблон3ToolStripMenuItem});
            this.создатьПоШалбону1ToolStripMenuItem.Name = "создатьПоШалбону1ToolStripMenuItem";
            this.создатьПоШалбону1ToolStripMenuItem.Size = new System.Drawing.Size(300, 22);
            this.создатьПоШалбону1ToolStripMenuItem.Text = "Создать по шалбон";
            // 
            // шаблон1ToolStripMenuItem
            // 
            this.шаблон1ToolStripMenuItem.Name = "шаблон1ToolStripMenuItem";
            this.шаблон1ToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.шаблон1ToolStripMenuItem.Text = "Шаблон №1";
            this.шаблон1ToolStripMenuItem.Click += new System.EventHandler(this.шаблон1ToolStripMenuItem_Click);
            // 
            // шаблон2ToolStripMenuItem
            // 
            this.шаблон2ToolStripMenuItem.Name = "шаблон2ToolStripMenuItem";
            this.шаблон2ToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.шаблон2ToolStripMenuItem.Text = "Шаблон №2";
            this.шаблон2ToolStripMenuItem.Click += new System.EventHandler(this.шаблон2ToolStripMenuItem_Click);
            // 
            // шаблон3ToolStripMenuItem
            // 
            this.шаблон3ToolStripMenuItem.Name = "шаблон3ToolStripMenuItem";
            this.шаблон3ToolStripMenuItem.Size = new System.Drawing.Size(141, 22);
            this.шаблон3ToolStripMenuItem.Text = "Шаблон №3";
            this.шаблон3ToolStripMenuItem.Click += new System.EventHandler(this.шаблон3ToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator3,
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.toolStripSeparator4,
            this.selectAllToolStripMenuItem,
            this.удалитьToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(108, 20);
            this.editToolStripMenuItem.Text = "Редактирование";
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.undoToolStripMenuItem.Text = "Назад";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.redoToolStripMenuItem.Text = "Вперёд";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(187, 6);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("cutToolStripMenuItem.Image")));
            this.cutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.cutToolStripMenuItem.Text = "Вырезать";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("copyToolStripMenuItem.Image")));
            this.copyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.copyToolStripMenuItem.Text = "Копировать";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("pasteToolStripMenuItem.Image")));
            this.pasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.pasteToolStripMenuItem.Text = "Вставить";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(187, 6);
            // 
            // selectAllToolStripMenuItem
            // 
            this.selectAllToolStripMenuItem.Name = "selectAllToolStripMenuItem";
            this.selectAllToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.A)));
            this.selectAllToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.selectAllToolStripMenuItem.Text = "Выделить все";
            this.selectAllToolStripMenuItem.Click += new System.EventHandler(this.selectAllToolStripMenuItem_Click);
            // 
            // удалитьToolStripMenuItem
            // 
            this.удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            this.удалитьToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.удалитьToolStripMenuItem.Size = new System.Drawing.Size(190, 22);
            this.удалитьToolStripMenuItem.Text = "Удалить";
            this.удалитьToolStripMenuItem.Click += new System.EventHandler(this.удалитьToolStripMenuItem_Click);
            // 
            // toolBar1
            // 
            this.toolBar1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.toolBar1.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.tblSave,
            this.tblUndo,
            this.tblNotUndo,
            this.tblDelete,
            this.tblHelp,
            this.tblNext,
            this.tblRetangle,
            this.tblRetangle1,
            this.tblArrow,
            this.tblDiamond,
            this.tblleroms});
            this.toolBar1.ButtonSize = new System.Drawing.Size(100, 100);
            this.toolBar1.DropDownArrows = true;
            this.toolBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(100)));
            this.toolBar1.ImageList = this.imageList1;
            this.toolBar1.Location = new System.Drawing.Point(0, 24);
            this.toolBar1.Name = "toolBar1";
            this.toolBar1.ShowToolTips = true;
            this.toolBar1.Size = new System.Drawing.Size(696, 34);
            this.toolBar1.TabIndex = 1;
            this.toolBar1.TabStop = true;
            this.toolBar1.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
            // 
            // tblSave
            // 
            this.tblSave.ImageIndex = 1;
            this.tblSave.Name = "tblSave";
            this.tblSave.ToolTipText = "Сохранить";
            // 
            // tblUndo
            // 
            this.tblUndo.ImageIndex = 4;
            this.tblUndo.Name = "tblUndo";
            this.tblUndo.ToolTipText = "Назад";
            // 
            // tblNotUndo
            // 
            this.tblNotUndo.ImageIndex = 3;
            this.tblNotUndo.Name = "tblNotUndo";
            this.tblNotUndo.ToolTipText = "Вперёд";
            // 
            // tblDelete
            // 
            this.tblDelete.ImageIndex = 0;
            this.tblDelete.Name = "tblDelete";
            this.tblDelete.ToolTipText = "Удалить";
            // 
            // tblHelp
            // 
            this.tblHelp.ImageIndex = 2;
            this.tblHelp.Name = "tblHelp";
            this.tblHelp.ToolTipText = "Помощь";
            // 
            // tblNext
            // 
            this.tblNext.ImageIndex = 5;
            this.tblNext.Name = "tblNext";
            // 
            // tblRetangle
            // 
            this.tblRetangle.ImageIndex = 6;
            this.tblRetangle.Name = "tblRetangle";
            // 
            // tblRetangle1
            // 
            this.tblRetangle1.ImageIndex = 7;
            this.tblRetangle1.Name = "tblRetangle1";
            // 
            // tblArrow
            // 
            this.tblArrow.ImageIndex = 8;
            this.tblArrow.Name = "tblArrow";
            // 
            // tblDiamond
            // 
            this.tblDiamond.ImageIndex = 9;
            this.tblDiamond.Name = "tblDiamond";
            // 
            // tblleroms
            // 
            this.tblleroms.ImageIndex = 10;
            this.tblleroms.Name = "tblleroms";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "deletered.png");
            this.imageList1.Images.SetKeyName(1, "document_save.png");
            this.imageList1.Images.SetKeyName(2, "help_browser.png");
            this.imageList1.Images.SetKeyName(3, "old_edit_redo.png");
            this.imageList1.Images.SetKeyName(4, "old_edit_undo.png");
            this.imageList1.Images.SetKeyName(5, "arowsline.png");
            this.imageList1.Images.SetKeyName(6, "rectangle.png");
            this.imageList1.Images.SetKeyName(7, "rectangle_blue.png");
            this.imageList1.Images.SetKeyName(8, "line_arrow_end.png");
            this.imageList1.Images.SetKeyName(9, "diamond.png");
            this.imageList1.Images.SetKeyName(10, "reloms.png");
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "diagram";
            this.saveFileDialog1.Filter = "Diagram|*.diagram";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "diagram";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Diagram|*.diagram";
            // 
            // saveFileDialog2
            // 
            this.saveFileDialog2.Filter = "PNG Image(*.png)|*.png|Metafile(*.emf)|*.emf";
            // 
            // controlDiagrama1
            // 
            this.controlDiagrama1.AutoScroll = true;
            this.controlDiagrama1.AutoScrollMargin = new System.Drawing.Size(2000, 2000);
            this.controlDiagrama1.AutoScrollMinSize = new System.Drawing.Size(2000, 2000);
            this.controlDiagrama1.AutoSize = true;
            this.controlDiagrama1.BackColor = System.Drawing.Color.White;
            this.controlDiagrama1.Diagram = null;
            this.controlDiagrama1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.controlDiagrama1.Location = new System.Drawing.Point(0, 58);
            this.controlDiagrama1.Name = "controlDiagrama1";
            this.controlDiagrama1.Size = new System.Drawing.Size(696, 388);
            this.controlDiagrama1.TabIndex = 2;
            this.controlDiagrama1.Load += new System.EventHandler(this.controlDiagrama1_Load_2);
            this.controlDiagrama1.Paint += new System.Windows.Forms.PaintEventHandler(this.controlDiagrama1_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(696, 446);
            this.Controls.Add(this.controlDiagrama1);
            this.Controls.Add(this.toolBar1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Activity";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Scroll += new System.Windows.Forms.ScrollEventHandler(this.Form1_Scroll);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem selectAllToolStripMenuItem;
        private System.Windows.Forms.ToolBar toolBar1;
        private System.Windows.Forms.ToolBarButton tblSave;
        private System.Windows.Forms.ToolBarButton tblUndo;
        private System.Windows.Forms.ToolBarButton tblNotUndo;
        private System.Windows.Forms.ToolBarButton tblDelete;
        private System.Windows.Forms.ToolBarButton tblHelp;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolBarButton tblNext;
        private System.Windows.Forms.ToolBarButton tblRetangle;
        private System.Windows.Forms.ToolBarButton tblRetangle1;
        private System.Windows.Forms.ToolBarButton tblArrow;
        private System.Windows.Forms.ToolBarButton tblDiamond;
        private System.Windows.Forms.ToolStripMenuItem выходToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog2;
        private System.Windows.Forms.ToolStripMenuItem удалитьToolStripMenuItem;
        private System.Windows.Forms.ToolBarButton tblleroms;
        private System.Windows.Forms.ToolStripMenuItem экспортВJMGСВодянымЗнакомToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выкладываниеФайлНаСерверToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem создатьПоШалбону1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem шаблон1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem шаблон2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem шаблон3ToolStripMenuItem;
        private ControlDiagrama controlDiagrama1;
    }
}

