﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;

namespace Activity
{
    [Serializable]
    public abstract class SolidFigure : Figure
    {
        protected static int defaultSize = 80;

        public static Brush brush = Brushes.White;

        public static Pen pens = new Pen(Color.Black, 2);

        public Point location;

        protected RectangleF textRect;
        public string text = null;

        public string textShow = null;

        protected virtual StringFormat StringFormat
        {
            get
            {
                StringFormat stringFormat = new StringFormat();
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;
                return stringFormat;
            }
        }


        public override bool IsInsidePoint(Point p)
        {
            return Path.IsVisible(p.X - location.X, p.Y - location.Y);
        }

        public RectangleF Bounds
        {
            get
            {
                RectangleF bounds = Path.GetBounds();
                return new RectangleF(bounds.Left + location.X, bounds.Top + location.Y, bounds.Width, bounds.Height);
            }
        }


        public Rectangle TextBounds
        {
            get
            {
                return new Rectangle((int)textRect.Left + location.X, (int)textRect.Top + location.Y, (int)textRect.Width, (int)textRect.Height);
            }
        }


        public SizeF Size
        {
            get { return Path.GetBounds().Size; }
            set
            {
                SizeF oldSize = Path.GetBounds().Size;
                SizeF newSize = new SizeF(Math.Max(1, value.Width), Math.Max(1, value.Height));

                float kx = newSize.Width / oldSize.Width;

                float ky = newSize.Height / oldSize.Height;
              //  Scale(kx, ky);
            }
        }


        //public void Scale(float scaleX, float scaleY)
        //{

        //    Matrix m = new Matrix();
        //    m.Scale(scaleX, scaleY);
        //    Path.Transform(m);

        //    textRect = new RectangleF(textRect.Left * scaleX, textRect.Top * scaleY, textRect.Width * scaleX, textRect.Height * scaleY);
        //}


        public virtual void Offset(int dx, int dy)
        {
            location.Offset(dx, dy);
            if (location.X < 0)
                location.X = 0;
            if (location.Y < 0)
                location.Y = 0;
        }


        public override void Draw(Graphics gr)
        {
            gr.TranslateTransform(location.X, location.Y);
            gr.FillPath(brush, Path);
            gr.DrawPath(pens, Path);
            if (!string.IsNullOrEmpty(text))
                gr.DrawString(text, SystemFonts.DefaultFont, Brushes.Black, textRect, StringFormat);
            gr.ResetTransform();
        }


        public override List<Marker> CreateMarkers(Diagram diagram)
        {
            List<Marker> markers = new List<Marker>();
            Marker m = new SizeMarker();
            m.targetFigure = this;
            markers.Add(m);

            return markers;
        }
    }
}