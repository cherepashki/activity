﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;

namespace Activity
{
    [Serializable]
    public class EndLineMarker : Marker
    {
        int pointIndex;
        Diagram diagram;

        public EndLineMarker(Diagram diagram, int pointIndex)
        {
            this.diagram = diagram;
            this.pointIndex = pointIndex;
        }

        public override void UpdateLocation()
        {
            AssocFigure line = (targetFigure as AssocFigure);
            if (line.From == null || line.To == null)
                return;

            SolidFigure figure = pointIndex == 0 ? line.From : line.To;
            location = figure.location;
        }

        public override void Offset(int dx, int dy)
        {

            base.Offset(dx, dy);

            SolidFigure figure = null;
            for (int i = diagram.figures.Count - 1; i >= 0; i--)
                if (diagram.figures[i] is SolidFigure && diagram.figures[i].IsInsidePoint(location))
                {
                    figure = (SolidFigure)diagram.figures[i];
                    break;
                }

            AssocFigure line = (targetFigure as AssocFigure);
            if (figure == null)
                figure = this;


            if (line.From == figure || line.To == figure)
                return;

            if (pointIndex == 0)
                line.From = figure;
            else
                line.To = figure;
        }
    }
}
