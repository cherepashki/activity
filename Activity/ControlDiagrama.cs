﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Printing;

namespace Activity
{
    public partial class ControlDiagrama : UserControl
    {
        
        Diagram diagram;
        public Figure SelectedFigure = null;
        Figure DragFigure = null;
        Point startDragPoint;
        public  List<Marker> market = new List<Marker>();
        List<Figure> selectedFigures = new List<Figure>();

        bool selectionStart = false;

        //нажата клавиша Ctrl
        bool canSelectFewFigures = false;
        //фигура была передвинута
        bool figureWasDragged = false;

        //Режим копирования фигуры
        bool copyStart;
        //Копируемая фигура
        bool successCopy;


        Pen selectRectPen;

        public List<Diagram> ShagUndo = new List<Diagram>();
        public List<Diagram> ShagRedo = new List<Diagram>();
        public ControlDiagrama()
        {
            InitializeComponent();
            DoubleBuffered = true;
            ResizeRedraw = true;
            selectRectPen = new Pen(Color.Blue, 1);
            selectRectPen.DashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
        }
        public void AddShagUndo()
        {
            Diagram newShag = new Diagram();
            newShag = Diagram.Klones(diagram);
            ShagUndo.Add(newShag);
            ShagRedo.Clear();
            Invalidate();
        }
        public void RedoClick(object sender, EventArgs e)
        {
            Invalidate();
            int count = ShagRedo.Count;
            if (count > 0)
            {
                ShagUndo.Add(diagram);
                diagram = ShagRedo[count - 1];
                ShagRedo.RemoveAt(count - 1);
                Diagram = diagram;
            }
            Invalidate();
        }
        public void UndoClick(object sender, EventArgs e)
        {
            Invalidate();
            int count = ShagUndo.Count;
            if (count > 0)
            {
                ShagRedo.Add(diagram);
                diagram = ShagUndo[count - 1];
                ShagUndo.RemoveAt(count - 1);
            }
            Invalidate();
        }
        public Diagram Diagram
        {
            get { return diagram; }
            set
            {
                diagram = value;
                SelectedFigure = null;
                DragFigure = null;
                market.Clear();
                Invalidate();
            }
        }
        private void BringFigureToFront(SolidFigure selectedFigure)
        {
            if (selectedFigure != null)
            {
                diagram.figures.Remove(selectedFigure);
                diagram.figures.Add(selectedFigure);
                Invalidate();
            }
        }
        private void SendFigureToBack()
        {
            if (SelectedFigure != null)
            {
                diagram.figures.Remove(SelectedFigure);
                diagram.figures.Insert(0, SelectedFigure);
                Invalidate();
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            Draw(e.Graphics);
        }
        public void Draw(Graphics gr)
        {
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            if (diagram != null)
            {
                foreach (Figure f in diagram.figures)
                    if (f is AssocFigure)
                        f.Draw(gr);
                foreach (Figure f in diagram.figures)
                    if (f is SolidFigure)
                        f.Draw(gr);
            }
            if (SelectedFigure is SolidFigure)
            {

                SolidFigure figure = SelectedFigure as SolidFigure;
                RectangleF bounds = figure.Bounds;
                gr.DrawRectangle(selectRectPen, bounds.Left - 2, bounds.Top - 2, bounds.Width + 4, bounds.Height + 4);
            }
            // chinh sua o day

            foreach (Figure figureInSelection in selectedFigures)
            {
                if (figureInSelection is SolidFigure)
                {
                    RectangleF boundsOfFigureInSelection = (figureInSelection as SolidFigure).Bounds;
                    gr.DrawRectangle(selectRectPen, boundsOfFigureInSelection.Left - 2, boundsOfFigureInSelection.Top - 2, boundsOfFigureInSelection.Width + 4, boundsOfFigureInSelection.Height + 4);
                }
            }


            foreach (Marker m in market)
            {
                if (SelectedFigure is AssocFigure)
                 m.Draw(gr);
                 
            }

        }
        public override Point AutoScrollOffset
        {
            get
            {
                return base.AutoScrollOffset;
            }
            set
            {
                base.AutoScrollOffset = value;
            }
        }
        public override bool AutoScroll
        {
            get
            {
                return base.AutoScroll;
            }
            set
            {
                base.AutoScroll = value;
            }
        }
        protected override void OnPaintBackground(PaintEventArgs e)
        {
           // e.Graphics.TranslateTransform(5, 5);
           
            base.OnPaintBackground(e);
        }
        Figure FindFigureByPoint(Point p)
        {
            foreach (Marker m in market)
                if (m.IsInsidePoint(p))
                    return m;

            for (int i = diagram.figures.Count - 1; i >= 0; i--)
                if (diagram.figures[i] is SolidFigure && diagram.figures[i].IsInsidePoint(p))
                    return diagram.figures[i];

            for (int i = diagram.figures.Count - 1; i >= 0; i--)
                if (diagram.figures[i] is AssocFigure && diagram.figures[i].IsInsidePoint(p))
                    return diagram.figures[i];
            return null;
        }
        private void UpdateMarkers()
        {
            foreach (Marker m in market)
                if (DragFigure != m)
                    m.UpdateLocation();
        }

        public void CreateMarkers()
        {
            if (SelectedFigure == null)
                market = new List<Marker>();
            else
            {
                market = SelectedFigure.CreateMarkers(diagram);
                UpdateMarkers();
            }
        }
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
            Focus();
            DragFigure = FindFigureByPoint(e.Location);
            if (!(DragFigure is Marker))
            {
                SelectedFigure = DragFigure;
              //  CreateMarkers();
            }
            if (!(DragFigure is Marker))
            {
                SelectedFigure = DragFigure;
                if (SelectedFigure != null)
                {
                    if (SelectedFigure != null && SelectedFigure is AssocFigure)
                    {
                        if (canSelectFewFigures == false)
                        {
                            foreach (Figure figure in diagram.figures)
                            {
                                if (figure is AssocFigure)
                                    (figure as AssocFigure).isBlue = false;

                            }
                        }
                       // (SelectedFigure as AssocFigure).isBlue = true;
                    }
                    else
                    {
                        foreach (Figure figure in diagram.figures)
                        {
                            if (figure is AssocFigure)
                            {
                               // (figure as AssocFigure).isBlue = true;
                            }
                        }
                    }
                    if (canSelectFewFigures == true)
                    {
                        if (!(selectedFigures.Contains(DragFigure)))
                            selectedFigures.Add(DragFigure);
                    }
                    else
                    {
                        if (SelectedFigure != null)
                        {
                            if(FewFiguresAreSelected() == false)
                                selectedFigures.Clear();
                            if(FewFiguresAreSelected() == true && figureWasDragged == false)
                                selectedFigures.Clear();
                        }
                        else selectedFigures.Clear();
                        //if (SelectedFigure != null)
                        //{ // sua o day
                        //}
                        //else
                        //    selectedFigures.Clear();
                    }

                }
                else
                {
                    selectedFigures.Clear();
                    foreach (Figure figure in diagram.figures)
                    {
                        if (figure is AssocFigure)
                        {
                            (figure as AssocFigure).isBlue = false;
                        }
                    }
                }
            }
            CreateMarkers();
            startDragPoint = e.Location;
            Invalidate();

            // я уже исправил здесь.
            // При нажим правую кнопку мыши, появится Menutrip 
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
                if (SelectedFigure != null)
                    contextMenuStrip1.Show(PointToScreen(e.Location));

        }
        protected override void OnKeyUp(KeyEventArgs e)
        {
            if (selectionStart)
            {
                selectionStart = false;
            }
            if (canSelectFewFigures == true)
            {
                canSelectFewFigures = false;
            }

            base.OnKeyUp(e);
        }
        void textBox_LostFocus(object sender, EventArgs e)
        {
            if (SelectedFigure != null && (SelectedFigure is SolidFigure))
            {
                AddShagUndo();
                (SelectedFigure as SolidFigure).text = (sender as TextBox).Text;
                if ((sender as TextBox).Text.Length > 80)
                {
                    string strsss = (sender as TextBox).Text.Substring(0, 80);
                    strsss += "...";
                    (SelectedFigure as SolidFigure).text = strsss;
                    (SelectedFigure as SolidFigure).textShow = (sender as TextBox).Text;
                }
                else
                {
                    (SelectedFigure as SolidFigure).text = (sender as TextBox).Text;
                    (SelectedFigure as SolidFigure).textShow = (sender as TextBox).Text;
                }
            }
            if (SelectedFigure != null && (SelectedFigure is AssocFigure))
            {
                AddShagUndo();
                if ((sender as TextBox).Text.Length > 20)
                {
                    (SelectedFigure as AssocFigure).text = (sender as TextBox).Text;
                    string strsss = (sender as TextBox).Text.Substring(0, 20);
                    strsss += "...";
                    (SelectedFigure as AssocFigure).text = strsss;
                    (SelectedFigure as AssocFigure).textShow = (sender as TextBox).Text;
                }
                else
                {
                    (SelectedFigure as AssocFigure).text = (sender as TextBox).Text;
                    (SelectedFigure as AssocFigure).textShow = (sender as TextBox).Text;
                }
            }
            Controls.Remove((Control)sender);
        }
        private void editTextToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TextBox textBox = new TextBox();
            textBox.Parent = this;
            if (SelectedFigure != null && (SelectedFigure is SolidFigure))
            {
                SolidFigure figure = (SelectedFigure as SolidFigure);
                
                textBox.SetBounds(figure.TextBounds.Left, figure.TextBounds.Top, figure.TextBounds.Width, figure.TextBounds.Height);
                textBox.Text = figure.textShow;
                textBox.Multiline = true;
               // textBox.Font = new Font(this.Font, FontStyle.Bold);
                textBox.TextAlign = HorizontalAlignment.Center;
               // textBox.MaxLength = 100;
                textBox.Focus();
                string teees = textBox.Text;
                if (textBox.Text.Length > 50 && !textBox.Focus())
                {
                  
                }
                else
                {
                    textBox.Text = teees;
                }
                textBox.LostFocus += new EventHandler(textBox_LostFocus);
            }
            else
                if (SelectedFigure is AssocFigure)
                {
                    AssocFigure figure = (SelectedFigure as AssocFigure);
                    textBox.SetBounds(Convert.ToInt16(figure.ledgePositionX), figure.To.location.Y - 20, 80, 40);
                    textBox.Text = figure.text;
                    textBox.Multiline = true;
                 //   textBox.Font = new Font(this.Font, FontStyle.Bold);
                    textBox.TextAlign = HorizontalAlignment.Left;
                    textBox.Focus();
                 //   textBox.MaxLength = 100;
                    textBox.LostFocus += new EventHandler(textBox_LostFocus);
                }
        }
        protected override void OnMouseDoubleClick(MouseEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            if (SelectedFigure != null)
                editTextToolStripMenuItem_Click(null, null);
            else
            {
               
            }
        }
        public void CreateMarker()
        {
            if (SelectedFigure != null)
                market = new List<Marker>();
            else
            {
                if (selectedFigures.Count == 0)
                    market = SelectedFigure.CreateMarkers(diagram);
                else
                {
                    foreach (Figure f in selectedFigures)
                    {
                        if (f is SolidFigure)
                            market = f.CreateMarkers(diagram);
                    }
                    
                }
                UpdateMarkers();
            }
        }
        //функция проверяет, выделенно ли несколько фигур на рабочей области
        protected bool FewFiguresAreSelected()
        {
            if (selectedFigures.Count < 2)
                return false;
            else
                return true;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            int dPosition = 0;
            base.OnMouseMove(e);
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (FewFiguresAreSelected() == true)
                {
                    //перетаскиваем наши фигуры, содержащиеся внутри выделения
                    foreach (Figure currentFigure in selectedFigures)
                    {
                        if (currentFigure is SolidFigure)
                        {
                            dPosition = (currentFigure as SolidFigure).location.X;
                            (currentFigure as SolidFigure).Offset(e.Location.X - startDragPoint.X, e.Location.Y - startDragPoint.Y);
                            dPosition = dPosition - (currentFigure as SolidFigure).location.X;
                            UpdateMarkers();
                            Invalidate();
                        }
                        else
                        {
                            if (currentFigure is LedgeLineFigure)
                            {
                                (currentFigure as LedgeLineFigure).ledgePositionX = (currentFigure as LedgeLineFigure).ledgePositionX - dPosition;
                            }
                        }
                        CreateMarkers();
                        UpdateMarkers();
                        Invalidate();
                    }
                }
                else
                {
                    if (DragFigure != null && (DragFigure is SolidFigure))
                    {
                        (DragFigure as SolidFigure).Offset(e.Location.X - startDragPoint.X, e.Location.Y - startDragPoint.Y);
                        UpdateMarkers();
                        Invalidate();
                    }
                }
                figureWasDragged = true;
            }
            else
            {
                Figure figure = FindFigureByPoint(e.Location);
                if (figure is Marker)
                    Cursor = Cursors.SizeAll;
                else
                    if (figure != null)
                        Cursor = Cursors.Hand;
                    else
                        Cursor = Cursors.Default;
            }
            startDragPoint = e.Location;
            //if (e.Button == System.Windows.Forms.MouseButtons.Left)
            //{
            //    if (DragFigure != null && (DragFigure is SolidFigure))
            //    {
            //        (DragFigure as SolidFigure).Offset(e.Location.X - startDragPoint.X, e.Location.Y - startDragPoint.Y);
            //        UpdateMarkers();
            //        Invalidate();
            //    }
            //}
            //else
            //{
            //    Figure figure = FindFigureByPoint(e.Location);
            //    if (figure is Marker)
            //        Cursor = Cursors.SizeAll;
            //    else
            //        if (figure != null)
            //            Cursor = Cursors.Hand;
            //        else
            //            Cursor = Cursors.Default;
            //}

            //startDragPoint = e.Location;
        }
        // chua su o cho nay
        protected override void OnMouseUp(MouseEventArgs e)
        {
            base.OnMouseUp(e);
            AddShagUndo();
            figureWasDragged = false;
            DragFigure = null;
           
            UpdateMarkers();
            Invalidate();
        }
        public void AddFigure<FigureType>(Point location) where FigureType : SolidFigure
        {
            AddShagUndo();
            FigureType figure = Activator.CreateInstance<FigureType>();
            figure.location = location;
            if (diagram != null)
                diagram.figures.Add(figure);
            Invalidate();
        }
        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
                удалитьОбъектToolStripMenuItem_Click(null, null);
            if (e.Control)
            {
                selectionStart = true;

                canSelectFewFigures = true;

                if (SelectedFigure != null && !selectedFigures.Contains(SelectedFigure))
                {
                    selectedFigures.Add(SelectedFigure);
                }
                else
                {
                   
                }
            }
            //else
            //    canSelectFewFigures = false;
            if (e.Alt)
            {
               
            }
            base.OnKeyDown(e);
        }
        public void selectAllFigures()
        {
            foreach (Figure figure in diagram.figures)
            {
                if (!(figure is Marker))
                {
                    selectedFigures.Add(figure);
                }
            }
            CreateMarkers();
            Invalidate();
        }
        private void DeleteFigure()
        {
            if (selectedFigures.Count == 0)
                selectedFigures.Add(SelectedFigure);
            foreach (Figure selecF in selectedFigures)
            {
                if (selecF != null)
                {
                    //удалем фигуру
                    diagram.figures.Remove(selecF);
                    //удялаем также все линии, ведущие к данной фигуре
                    for (int i = diagram.figures.Count - 1; i >= 0; i--)
                        if (diagram.figures[i] is AssocFigure)
                        {
                            AssocFigure line = (diagram.figures[i] as AssocFigure);
                            if (line.To == selecF || line.From == selecF)
                                diagram.figures.RemoveAt(i);
                        }
                }
            }
            SelectedFigure = null;
            DragFigure = null;
            selectedFigures.Clear();
            selectionStart = false;
            CreateMarkers();
            Invalidate();
        }
        public void CutFigure(object sender, EventArgs e)
        {
            CopyToClipBoard();
            DeleteFigure();
            Invalidate();
        }
        public void CopyToClipBoard()
        {
            
            DataFormats.Format format = DataFormats.GetFormat(typeof(List<Figure>).FullName);
            IDataObject dataObj = new DataObject();
            if (selectedFigures.Count == 0 && SelectedFigure != null)
                selectedFigures.Add(SelectedFigure);
            if (selectedFigures.Count != 0)
            {
                Clipboard.SetData("mass", selectedFigures);
            }
        }
        public void CopyToWordClipBoard()
        {
            
            DataFormats.Format format = DataFormats.GetFormat(typeof(List<Figure>).FullName);
            
            IDataObject dataObj = new DataObject();
            if (selectedFigures.Count == 0 && SelectedFigure != null)
                selectedFigures.Add(SelectedFigure);
            if (selectedFigures.Count != 0)
            {
                
                int fromX = Bounds.Width, fromY = Bounds.Height, toX = 0, toY = 0;
                
                foreach (Figure f in selectedFigures)
                {
                    if (f is SolidFigure)
                    {
                        if (((f as SolidFigure).location.X - (f as SolidFigure).Bounds.Width / 2) < fromX)
                        {
                            fromX = (int)((float)(f as SolidFigure).location.X - (f as SolidFigure).Bounds.Width / 2);
                        }
                        if (((f as SolidFigure).location.X + (f as SolidFigure).Bounds.Width / 2) > toX)
                        {
                            toX = (int)((float)(f as SolidFigure).location.X + (f as SolidFigure).Bounds.Width / 2);
                        }
                        if (((f as SolidFigure).location.Y - (f as SolidFigure).Bounds.Height / 2) < fromY)
                        {
                            fromY = (int)((float)(f as SolidFigure).location.Y - (f as SolidFigure).Bounds.Height / 2);
                        }
                        if (((f as SolidFigure).location.Y + (f as SolidFigure).Bounds.Height / 2) > toY)
                        {
                            toY = (int)((float)(f as SolidFigure).location.Y + (f as SolidFigure).Bounds.Width / 2);
                        }
                    }
                    if (f is LedgeLineFigure)
                    {
                        if ((f as LedgeLineFigure).ledgePositionX < fromX)
                            fromX = (int)((f as LedgeLineFigure).ledgePositionX - 5);
                        if ((f as LedgeLineFigure).ledgePositionX > toX)
                            toX = (int)((f as LedgeLineFigure).ledgePositionX + 5);
                    }
                }
               
                Bitmap bmpLarge = new Bitmap(Bounds.Width, Bounds.Height);
                Bitmap bmp = new Bitmap(toX - fromX + 4, toY - fromY);
                this.DrawToBitmap(bmpLarge, new Rectangle(0, 0, Bounds.Width, Bounds.Height));
                
                bmp = bmpLarge.Clone(new Rectangle(fromX - 3, fromY - 2, toX - fromX + 4, (toY - fromY) - 14), bmpLarge.PixelFormat);
                Clipboard.SetImage(bmp);
                canSelectFewFigures = false;
            }
        }

        //Берём обратно
        protected static List<Figure> GetFromClipboard()
        {
            List<Figure> dataObj = new List<Figure>();
            if (Clipboard.ContainsData("mass"))
                dataObj = Clipboard.GetData("mass") as List<Figure>;
            return dataObj;
        }

        public Bitmap GetImage()
        {
            SelectedFigure = null;
            DragFigure = null;
            CreateMarkers();

            Bitmap bmp = new Bitmap(Bounds.Width, Bounds.Height);
            DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));

            return bmp;
        }

        public void Paste()
        {
            foreach (Figure figure in GetFromClipboard())
            {
                diagram.figures.Add(figure);
                SelectedFigure = figure;
                arrowMove(20, 20);
            }
            Invalidate();
        }

        public void SaveAsMetafile(string fileName)
        {
            SelectedFigure = null;
            DragFigure = null;
            CreateMarkers();

            Metafile curMetafile = null;
            Graphics g = this.CreateGraphics();
            IntPtr hdc = g.GetHdc();
            Rectangle rect = new Rectangle(0, 0, 200, 200);
            try
            {
                curMetafile =
                    new Metafile(fileName, hdc, System.Drawing.Imaging.EmfType.EmfOnly);
            }
            catch
            {
                g.ReleaseHdc(hdc);
                g.Dispose();
                throw;
            }

            Graphics gr = Graphics.FromImage(curMetafile);

            Draw(gr);

            g.ReleaseHdc(hdc);

            gr.Dispose();
            g.Dispose();
        }
        // Удалить объект
        public void удалитьОбъектToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (selectedFigures.Count == 0)
                selectedFigures.Add(SelectedFigure);
            foreach (Figure selecF in selectedFigures)
            {
                if (selecF != null)
                {
                    //удалем фигуру
                   diagram.figures.Remove(selecF);
                    //удялаем также все линии, ведущие к данной фигуре
                   for (int i = diagram.figures.Count - 1; i >= 0; i--)
                       if (diagram.figures[i] is AssocFigure)
                        {
                            AssocFigure line = (diagram.figures[i] as AssocFigure);
                            if (line.To == selecF || line.From == selecF)
                                diagram.figures.RemoveAt(i);
                        }
                }
            }
            SelectedFigure = null;
            DragFigure = null;
            selectedFigures.Clear();
            selectionStart = false;
            CreateMarkers();
            Invalidate();
            //if (SelectedFigure != null)
            //{
            //    AddShagUndo();
            //    diagram.figures.Remove(SelectedFigure);
            //    for(int i = diagram.figures.Count - 1; i >= 0; i--)
            //        if (diagram.figures[i] is AssocFigure)
            //        {
            //            AssocFigure line = (diagram.figures[i] as AssocFigure);
            //            if (line.To == SelectedFigure || line.From == SelectedFigure)
            //                diagram.figures.RemoveAt(i);
            //        }
            //    SelectedFigure = null;
            //    DragFigure = null;
            //    CreateMarkers();
            //    Invalidate();
            //}
        }
        private void arrowMove(int offsetX, int offsetY)
        {
            if (SelectedFigure != null && SelectedFigure is SolidFigure)
            {
               (SelectedFigure as SolidFigure).Offset(offsetX, offsetY);
                UpdateMarkers();
                Invalidate();
            }
        }
        private void massArrowMove(int offsetX, int offsetY)
        {
            foreach (Figure figure in selectedFigures)
            {
                if (figure != null && figure is SolidFigure)
                {
                    (figure as SolidFigure).Offset(offsetX, offsetY);
                    UpdateMarkers();
                    Invalidate();
                }
            }
        }
        public int CountFiguresNows()
        {
            int coutsss = 0;
            for (int i = diagram.figures.Count - 1; i >= 0; i--)
            {
                if (diagram.figures[i] is SolidFigure)
                {
                    coutsss++;
                }
            }
            return coutsss;
        }
       
        
        private void выходToolStripMenuItem_DropDownOpened(object sender, EventArgs e)
        {

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void ControlDiagrama_Load(object sender, EventArgs e)
        {
           
            this.Width = 2000;
            this.Height = 2000;
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Hide();
        }

        private void ControlDiagrama_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void ControlDiagrama_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

        }

        private void ControlDiagrama_ControlAdded(object sender, ControlEventArgs e)
        {
            
        }

        private void ControlDiagrama_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void ControlDiagrama_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == System.Windows.Forms.Keys.A)
            {

            }
        }

        private void ControlDiagrama_Scroll(object sender, ScrollEventArgs e)
        {
            Point p = new Point();
            p = this.AutoScrollPosition;
           // this.CreateMarker();
            this.UpdateMarkers();
            //Graphics gr = this.CreateGraphics();
           // gr.TranslateTransform(p.X, p.Y);
           // Invalidate();
        }

        private void ControlDiagrama_AutoValidateChanged(object sender, EventArgs e)
        {

            Invalidate();
        }
       
    }
}
