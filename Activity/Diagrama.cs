﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;

namespace Activity
{
    [Serializable]
    public class Diagram
    {
        public readonly List<Figure> figures = new List<Figure>();

        public void Save(string fileName)
        {
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Create))
                    new BinaryFormatter().Serialize(fs, this);
            }
            catch {}
        }


        public static Diagram Load(string fileName)
        {
            try
            {

                using (FileStream fs = new FileStream(fileName, FileMode.Open))
                    return (Diagram)new BinaryFormatter().Deserialize(fs);
            }
            catch { return null; }
        }
        public static Diagram Klones(Diagram ob)
        {
            Diagram ResObject = null;

            using (MemoryStream mstr = new MemoryStream())
            {
                BinaryFormatter binaryFomatter = new BinaryFormatter();
                binaryFomatter.Serialize(mstr, ob);
                mstr.Position = 0;
                ResObject = (Diagram)binaryFomatter.Deserialize(mstr);
            }
            return ResObject;
        }
    }
}
