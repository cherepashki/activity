﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
namespace Activity
{
     [Serializable]
    public class AssocFigure : Figure
    {
        public bool isBlue = false;
        public float ledgePositionX = -1;
        public SolidFigure From;
        public SolidFigure To;
        static Pen ClPen = new Pen(Color.Transparent, 2);
        static Pen clickPen = new Pen(Color.Transparent, 2);
        static Pen linePen = new Pen(Color.Black, 2);
        static Pen bluePen = new Pen(Color.Blue, 2);
        static AdjustableArrowCap ArrowCap = new AdjustableArrowCap(3, 5, true);
        public string text = null;
        protected RectangleF textRect;
        public string textShow = null;

         public override void Draw(Graphics gr)
        {
            if (From == null || To == null)
                return;

            RecalcToPath();


            linePen.CustomEndCap = ArrowCap;
            bluePen.CustomEndCap = ArrowCap;

            if (isBlue)
                gr.DrawPath(bluePen, Path);
            else
                gr.DrawPath(linePen, Path);
             
            
          
            
            PointF point = new PointF(); point.X = ledgePositionX; point.Y = To.location.Y;
         
            {
                if (From.location.Y < To.location.Y)
                    point.Y = From.location.Y - (From.location.Y - To.location.Y) / 2 + 10;
                else
                    point.Y = To.location.Y - (To.location.Y - From.location.Y) / 2 + 10;
                if (From.location.Y == To.location.Y)
                    point.Y = To.location.Y + 10;
            }       
            gr.DrawString(text, SystemFonts.DefaultFont, Brushes.Black, point);
            
        }
        public override bool IsInsidePoint(Point p)
        {
            if (From == null || To == null)
                return false;
            RecalcToPath();
            return Path.IsOutlineVisible(p , ClPen);
            

        }
        protected virtual void RecalcToPath()
        {
            
            PointF[] points = null;

            ledgePositionX = (From.location.X + To.location.X) / 2 + 10;
            if (Path.PointCount > 0)
                points = Path.PathPoints;
            if (Path.PointCount != 2 || points[0] != From.location || points[1] != To.location)
            {
                //Рисуем связи между объектами
                Path.Reset();
                if (From.location.X == To.location.X)
                {
                    Path.AddLine(From.location, To.location);
                }
                if (From.location.Y <= To.location.Y - To.Bounds.Height)
                {
                    if (From.location.X < To.location.X - To.Bounds.Width / 3)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.location.X - To.Bounds.Width / 3, To.Bounds.Top) });
                    }
                    if (From.location.X > To.location.X - To.Bounds.Width / 3 && From.location.X < To.location.X + To.Bounds.Width / 3)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.location.X, To.Bounds.Top) });
                    }
                    if (From.location.X > To.location.X + To.Bounds.Width / 3)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.location.X + To.Bounds.Width / 3, To.Bounds.Top) });
                    }
                }

                if (From.location.X <= To.location.X - To.Bounds.Width)
                {

                    if (From.location.Y > To.location.Y - To.Bounds.Height && From.location.Y < To.location.Y - To.Bounds.Height / 3)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.Bounds.Left, To.location.Y - To.Bounds.Height / 3) });
                    }
                    if (From.location.Y > To.location.Y - To.Bounds.Height / 3 && From.location.Y < To.location.Y + To.Bounds.Height / 3)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.Bounds.Left, To.location.Y) });
                    }
                    if (From.location.Y > To.location.Y + To.Bounds.Height / 3 && From.location.Y < To.location.Y + To.Bounds.Height)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.Bounds.Left, To.location.Y + To.Bounds.Height / 3) });
                    }
                }
                if (From.location.X > To.location.X + To.Bounds.Width)
                {

                    if (From.location.Y > To.location.Y - To.Bounds.Height && From.location.Y < To.location.Y - To.Bounds.Height / 3)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.Bounds.Right, To.location.Y - To.Bounds.Height / 3) });
                    }
                    if (From.location.Y > To.location.Y - To.Bounds.Height / 3 && From.location.Y < To.location.Y + To.Bounds.Height / 3)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.Bounds.Right, To.location.Y) });
                    }
                    if (From.location.Y > To.location.Y + To.Bounds.Height / 3 && From.location.Y < To.location.Y + To.Bounds.Height)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.Bounds.Right, To.location.Y + To.Bounds.Height / 3) });
                    }
                }
                if (From.location.Y > To.location.Y + To.Bounds.Height)
                {

                    if (From.location.X < To.location.X - To.Bounds.Width / 3)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.location.X - To.Bounds.Width / 3, To.Bounds.Bottom) });
                    }
                    if (From.location.X > To.location.X - To.Bounds.Width / 3 && From.location.X < To.location.X + To.Bounds.Width / 3)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.location.X, To.Bounds.Bottom) });
                    }
                    if (From.location.X > To.location.X + To.Bounds.Width / 3)
                    {
                        Path.AddLines(new PointF[] { From.location, new PointF(To.location.X + To.Bounds.Width / 3, To.Bounds.Bottom) });
                    }
                }
            }
       
        }
        public override List<Marker> CreateMarkers(Diagram diagram)
        {
            List<Marker> markets = new List<Marker>();
            EndLineMarker m1 = new EndLineMarker(diagram, 0);
            m1.targetFigure = this;
            EndLineMarker m2 = new EndLineMarker(diagram, 1);
            m2.targetFigure = this;
            markets.Add(m1);
            markets.Add(m2);
            return markets;
        }
    }
     [Serializable]
     public class LinePerelom : AssocFigure
     {
         static Pen linePen = new Pen(Color.Black, 2);
         static Pen bluePen = new Pen(Color.Blue, 2);
         public override void Draw(Graphics gr)
         {
             if (From == null || To == null)
                 return;

             RecalcToPath();

             if (isBlue)
                 gr.DrawPath(bluePen, Path);
             else
                 gr.DrawPath(linePen, Path);




             PointF point = new PointF(); point.X = ledgePositionX; point.Y = To.location.Y;

             {
                 if (From.location.Y < To.location.Y)
                     point.Y = From.location.Y - (From.location.Y - To.location.Y) / 2 + 10;
                 else
                     point.Y = To.location.Y - (To.location.Y - From.location.Y) / 2 + 10;
                 if (From.location.Y == To.location.Y)
                     point.Y = To.location.Y + 10;
             }
             gr.DrawString(text, SystemFonts.DefaultFont, Brushes.Black, point);

         }

         protected override void RecalcToPath()
         {


             PointF[] points = null;

             if (ledgePositionX < 0)
                 ledgePositionX = (From.location.X + 10);

             if (Path.PointCount > 0)
                 points = Path.PathPoints;
             //if (Path.PointCount != 4 || points[0] != From.location || points[3] != To.location ||
             //    points[1].X != ledgePositionX)
             {
                 Path.Reset();
                 if (From.location.X < To.location.X && From.location.Y > To.location.Y)
                 {
                     Path.AddLines(new PointF[]{
                         From.location,
                         new PointF(From.location.X, From.location.Y + 200),
                         new PointF((From.location.X + To.location.X) / 2,From.location.Y + 200),
                         new PointF(To.location.X, From.location.Y + 200),
                         To.location
                     });
                 }
                 else if (From.location.X >= To.location.X &&From.location.Y >= To.location.Y )
                 {
                     Path.AddLines(new PointF[]{
                         From.location,
                         new PointF(From.location.X, From.location.Y + 200),
                         new PointF((From.location.X + To.location.X) / 2,From.location.Y + 200),
                         new PointF(To.location.X, From.location.Y + 200),
                         To.location
                     });
                 }
                 else if (From.location.X < To.location.X && From.location.Y <= To.location.Y)
                 {
                     Path.AddLines(new PointF[]{
                         From.location,
                         new PointF(From.location.X, From.location.Y - 200),
                         new PointF((From.location.X + To.location.X) / 2,From.location.Y - 200),
                         new PointF(To.location.X, From.location.Y - 200),
                         To.location
                     });
                 }
                 else if (From.location.X >= To.location.X && From.location.Y <= To.location.Y)
                 {
                     Path.AddLines(new PointF[]{
                         From.location,
                         new PointF(From.location.X, From.location.Y - 200),
                         new PointF((From.location.X + To.location.X) / 2,From.location.Y - 200),
                         new PointF(To.location.X, From.location.Y - 200),
                         To.location
                     });
                 }
                 
             }     
         }
         public override List<Marker> CreateMarkers(Diagram diagram)
         {
             RecalcToPath();
             List<Marker> markers = new List<Marker>();
             EndLineMarker m1 = new EndLineMarker(diagram, 0);
             m1.targetFigure = this;
             EndLineMarker m2 = new EndLineMarker(diagram, 1);
             m2.targetFigure = this;
             LedgeLineMarker m3 = new LedgeLineMarker();
             m3.targetFigure = this;
             m3.UpdateLocation();

             markers.Add(m1);
             markers.Add(m2);
             markers.Add(m3);

             return markers;
         }
     }
    [Serializable]
    public class LedgeLineFigure : AssocFigure
    {
        static Pen linePen = new Pen(Color.Black, 2);
        static Pen bluePen = new Pen(Color.Blue, 2);
        static AdjustableArrowCap ArrowCap = new AdjustableArrowCap(3, 5, true);
      
        protected override void  RecalcToPath()
        {
           
            PointF[] points = null;

            if (ledgePositionX < 0)
                ledgePositionX = (From.location.X + To.location.X) / 2;

            if (Path.PointCount > 0)
                points = Path.PathPoints;
            if (Path.PointCount != 4 || points[0] != From.location || points[3] != To.location ||
                points[1].X != ledgePositionX)
            {
                Path.Reset();
                if (ledgePositionX < To.location.X - To.Bounds.Width / 2)
                {
                    Path.AddLines(new PointF[]{
                    From.location,
                    new PointF(ledgePositionX, From.location.Y),
                    new PointF(ledgePositionX, To.location.Y),
                    new PointF(To.Bounds.Left, To.location.Y)
                    });
                }
                else
                {
                    if (ledgePositionX > To.location.X + To.Bounds.Width / 2)
                    {
                        Path.AddLines(new PointF[]{
                        From.location,
                        new PointF(ledgePositionX, From.location.Y),
                        new PointF(ledgePositionX, To.location.Y),
                        new PointF(To.Bounds.Right, To.location.Y)
                        });
                    }
                    else
                    {
                        if (To.location.Y > From.location.Y)
                        {
                            Path.AddLines(new PointF[]{
                            From.location,
                            new PointF(ledgePositionX, From.location.Y),
                            new PointF(ledgePositionX, To.Bounds.Top),
                            });
                        }
                        else
                        {
                            if (To.location.Y < From.location.Y)
                            {
                                Path.AddLines(new PointF[]{
                                From.location,
                                new PointF(ledgePositionX, From.location.Y),
                                new PointF(ledgePositionX, To.Bounds.Bottom),
                                });
                            }
                            else
                            {
                                Path.AddLines(new PointF[]{
                                From.location,
                                new PointF(ledgePositionX, From.location.Y),
                                new PointF(ledgePositionX, To.location.Y),
                                To.location
                                });
                            }
                        }
                    }
                }
            }           
        }

        public override List<Marker> CreateMarkers(Diagram diagram)
        {
            RecalcToPath();
            List<Marker> markers = new List<Marker>();
            EndLineMarker m1 = new EndLineMarker(diagram, 0);
            m1.targetFigure = this;
            EndLineMarker m2 = new EndLineMarker(diagram, 1);
            m2.targetFigure = this;
            LedgeMarker m3 = new LedgeMarker();
            m3.targetFigure = this;
            m3.UpdateLocation();

            markers.Add(m1);
            markers.Add(m2);
            markers.Add(m3);

            return markers;
        }
    }
}
