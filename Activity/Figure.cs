﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;

namespace Activity
{
    [Serializable]
  public abstract  class Figure
    {
        readonly SerializableGraphicsPath serializablePath = new SerializableGraphicsPath();
        protected GraphicsPath Path { get { return serializablePath.path; } }

        public static Pen pen = Pens.Black;

        public abstract bool IsInsidePoint(Point p);


        public abstract void Draw(Graphics gr);

        public abstract List<Marker> CreateMarkers(Diagram diagram);
        
    }
}
