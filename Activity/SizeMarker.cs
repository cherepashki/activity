﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;
namespace Activity
{
    [Serializable]
    public class SizeMarker : Marker
    {
        public override void UpdateLocation()
        {
            RectangleF bounds = (targetFigure as SolidFigure).Bounds;
            location = new Point((int)Math.Round(bounds.Right) + defaultSize / 2, (int)Math.Round(bounds.Bottom) + defaultSize / 2);
        }

        public override void Offset(int dx, int dy)
        {
            base.Offset(dx, dy);            
        }
    }
    [Serializable]
    public class LedgeMarker : Marker
    {
        public override void UpdateLocation()
        {
            LedgeLineFigure line = (targetFigure as LedgeLineFigure);
            if (line.From == null || line.To == null)
                return;
            //фигура, с которой связана линия
            location = new Point((int)line.ledgePositionX, (int)(line.From.location.Y + line.To.location.Y) / 2);
        }

        public override void Offset(int dx, int dy)
        {
            base.Offset(dx, 0);
            (targetFigure as LedgeLineFigure).ledgePositionX += dx;
        }
    }
    [Serializable]
    public class LedgeLineMarker : Marker
    {
        public override void UpdateLocation()
        {
            LinePerelom line = (targetFigure as LinePerelom);
            if (line.From == null || line.To == null)
                return;
            //фигура, с которой связана линия
            location = new Point((int)line.ledgePositionX, (int)(line.From.location.Y + line.To.location.Y) / 2);
        }

        public override void Offset(int dx, int dy)
        {
            base.Offset(dx, 0);
            (targetFigure as LinePerelom).ledgePositionX += dx;
        }
    }
}
