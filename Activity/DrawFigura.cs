﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Runtime.Serialization;
namespace Activity
{
    [Serializable]
    class DrawFigura : SolidFigure
    {

    }
    [Serializable]
    public class RectangleFigure : SolidFigure
    {
        public RectangleFigure()
        {
            Path.AddRectangle(new RectangleF(-defaultSize, -defaultSize / 2, defaultSize * 2, defaultSize));
            textRect = new RectangleF(-defaultSize / 1.4f, -defaultSize / 2 / 1.4f, 2 * defaultSize / 1.4f, defaultSize / 1.4f);
        }


    }
    [Serializable]
    public class RoundRectFigure : SolidFigure
    {
        public RoundRectFigure()
        {
            float diameter = 16f;
            SizeF sizeF = new SizeF(diameter, diameter);
            RectangleF arc = new RectangleF(-defaultSize, -defaultSize / 2, sizeF.Width, sizeF.Height);
            Path.AddArc(arc, 180, 90);
            arc.X = defaultSize - diameter;
            Path.AddArc(arc, 270, 90);
            arc.Y = defaultSize / 2 - diameter;
            Path.AddArc(arc, 0, 90);
            arc.X = -defaultSize;
            Path.AddArc(arc, 90, 90);
            Path.CloseFigure();

            textRect = new RectangleF(-defaultSize + 3, -defaultSize / 2 + 2, 2 * defaultSize - 6, defaultSize - 4);
        }
    }
    [Serializable]
    public class RhombFigure : SolidFigure
    {
        public RhombFigure()
        {
            // Добавляем в Path полигоны с определенными точками
            Path.AddPolygon(new PointF[]{
                new PointF(-defaultSize/5, 0),
                new PointF(0, -defaultSize/5),
                new PointF(defaultSize/5, 0),
                new PointF(0, defaultSize/5)
            });
            //  textRect = new RectangleF(-defaultSize / 2, -defaultSize / 4, defaultSize, defaultSize / 2);
        }
    }
}
