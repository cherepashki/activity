﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Drawing;

namespace Activity
{
    public class FormSettingStore
    {

        private string regPath;
        private string formName;
        private RegistryKey key;
        private string Catalogs = null;
        public string RegistryPath
        {
            get { return regPath; }
        }

        public string FormName
        {
            get { return formName; }
        }

        public FormSettingStore(string registryPath, string formName)
        {

            this.regPath = registryPath;
            this.formName = formName;

            key = Registry.LocalMachine.CreateSubKey(
             registryPath + formName);
        }

        public void SaveSettings(System.Windows.Forms.Form form, string catalog)
        {
            try
            {
                key.SetValue("Height", form.Height);
                key.SetValue("Width", form.Width);
                key.SetValue("Left", form.Left);
                key.SetValue("Top", form.Top);
                if (catalog != null)
                {
                    Catalogs = catalog;
                    key.SetValue("Catalog", (string)Catalogs);
                }
                else
                {
                    Catalogs = "";
                    key.SetValue("Catalog", (string)Catalogs);
                }
            }
            catch { }
        }

        public void ApplySettings(System.Windows.Forms.Form form, string catalog)
        {
            form.StartPosition = FormStartPosition.Manual;
            form.Height = (int)key.GetValue("Height", form.Height);
            form.Width = (int)key.GetValue("Width", form.Width);
            form.Left = (int)key.GetValue("Left", form.Left);
            form.Top = (int)key.GetValue("Top", form.Top);
            catalog = (string)key.GetValue("Catalog", Catalogs);
            if (catalog == "" || catalog == null)
            {
                  MessageBox.Show("No catalog...", "Warning");
            }
            else { MessageBox.Show(catalog, "Catalog last...."); }
                
            Catalogs = null;
        }
    }
}
